from mergexp import *

net = Network('triforce')

# Topology

def px(name):
    return net.node(
        name,
        memory.capacity >= gb(1),
        proc.cores >= 1,
        disk.capacity >= gb(100),
        metal == True
    )

def vx(name):
    return net.node(
        name,
        memory.capacity >= gb(1),
        proc.cores >= 4,
        disk.capacity >= gb(100),
    )

p = [px('p%d'%i) for i in range(3)]

p0_p1 = net.connect([p[0], p[1]], link.capacity == mbps(30))
p0_p2 = net.connect([p[0], p[2]], link.capacity == mbps(30))
p1_p2 = net.connect([p[1], p[2]], link.capacity == mbps(30))

v = [vx('v%d'%i) for i in range(9)]
p0_lan = net.connect([p[0]] + v[0:3], link.capacity == mbps(1))
p1_lan = net.connect([p[1]] + v[3:6], link.capacity == mbps(1))
p2_lan = net.connect([p[2]] + v[6:9], link.capacity == mbps(1))

# Addresses

p0_p1[p[0]].socket.addrs = ip4('1.0.0.10/24')
p0_p1[p[1]].socket.addrs = ip4('1.0.0.11/24')
p0_p2[p[0]].socket.addrs = ip4('1.0.1.10/24')
p0_p2[p[2]].socket.addrs = ip4('1.0.1.12/24')
p1_p2[p[1]].socket.addrs = ip4('1.0.2.11/24')
p1_p2[p[2]].socket.addrs = ip4('1.0.2.12/24')

for (i,e) in enumerate(p0_lan.endpoints, 1):
    e.socket.addrs.extend(ip4('10.0.0.%d/16'%i))

for (i,e) in enumerate(p1_lan.endpoints, 1):
    e.socket.addrs.extend(ip4('10.0.1.%d/16'%i))

for (i,e) in enumerate(p2_lan.endpoints, 1):
    e.socket.addrs.extend(ip4('10.0.2.%d/16'%i))


xx = net.xir()


experiment(net)
